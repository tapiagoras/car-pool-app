//
//  main.c
//  MidTerm_Tapia
//
//  Created by Fernando Tapia T on 10/10/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    // insert code here...
    int age=0,yearsOld=241,currentYear=2016;
    int flag=1;
    
    while (flag==1) {
        if(age==yearsOld){
            flag=0;
        }else{
            age++;
            printf("America is %d \tyears of age in the year of %d \n",age,currentYear-yearsOld+age);
        }
    }
    return 0;
}

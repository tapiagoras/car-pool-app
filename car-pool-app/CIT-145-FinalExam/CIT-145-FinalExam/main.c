//
//  main.c
//  CIT-145-FinalExam
//
//  Created by Fernando Tapia T on 12/12/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>
#include <ctype.h>
#include <string.h>

double bonusShift(int hours);

int main(int argc, const char * argv[]) {
    // Declare variables.
    char jobGroup;
    //const char jobGroupArr[3]={'A','B','C'};
    const int jobGrade[3]={100,200,300};
    int basicPay=0;
    char marriedStatus;
    int hours;
    double bonusShiftdata=0;
    // insert code here...
    float houseAllowance = 5000; //All employees get five thousand house allowance
    float marriageAllowance=0;
    float marriageAllowancePercent = 5; //give five percent discount on the gross for married employees
   
    //printf("%c", jobGroupArr[1]);
    
    do{
        printf("Hello, What's your job Group?(A,B OR C) --> ");
    scanf("%s", &jobGroup);
        jobGroup=toupper(jobGroup);
    }while(strncmp(&jobGroup, "A", 2)!=0 && strncmp(&jobGroup, "B", 2)!=0 && strncmp(&jobGroup, "C", 2)!=0);
    do{
        printf("\nAre you married? (Y or N) --> ");
    scanf("%s", &marriedStatus);
        marriedStatus=toupper(marriedStatus);
    }while(strncmp(&marriedStatus, "Y", 2)!=0 && strncmp(&marriedStatus, "N", 2)!=0);
    
    printf("\nHow many hours have you worked? --> ");
    scanf("%d", &hours);
    
    switch(jobGroup){
        case 'A':{basicPay=hours*jobGrade[0];
            break;
        }
        case 'B':{basicPay=hours*jobGrade[1];
            break;
        }
        case 'C':{basicPay=hours*jobGrade[2];
            break;
        }
            
        
    }
    
    
    printf("\nYour Basic pay is %d",basicPay);
    bonusShiftdata=bonusShift(hours);
    printf("\nYour Hours bonus is %.2f",bonusShiftdata);
   
    printf("\nYour House allowance is %.2f",houseAllowance);
    if (marriedStatus=='Y'){
        marriageAllowance = basicPay * (marriageAllowancePercent / 100);
         printf("\nYour Marriage allowance is %.2f",marriageAllowance);
    }
    float grossPay=bonusShiftdata+houseAllowance+basicPay+marriageAllowance;
    
    printf("\nYour Gross Pay is %.2f",grossPay);
    
    printf("\nYour Tax deduction is %.2f",grossPay/10);
    
    printf("\nYour Net Pay is %.2f\n",grossPay-grossPay/10);
    
    
    
    return 0;
}

double bonusShift(int hours){
    return  (hours * 2000) / 15 ;
}

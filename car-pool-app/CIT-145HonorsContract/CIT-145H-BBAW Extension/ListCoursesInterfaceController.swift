//
//  ListCoursesInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

//This class control the Courses Interface
class ListCoursesInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBOutlet var CourseList: WKInterfaceTable!
    
    var courses = Course.allCourses()//get all the courses per user
    var selectedIndex = 0
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // this will populate the table CoursesRow with each course from the course class
        CourseList.setNumberOfRows(courses.count, withRowType: "CoursesRow")
        for index in 0..<CourseList.numberOfRows {
            guard let controller = CourseList.rowController(at: index) as? CourseRowController else { continue }
            controller.course = courses[index]
        }
    }
    // This function invokes the InnerMenu interface and passed the course data in the context
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        let course = courses[rowIndex]
        let controllers = ["InnerMenu"]
        selectedIndex = rowIndex
        presentController(withNames: controllers, contexts: [course, course])
    }
    
}

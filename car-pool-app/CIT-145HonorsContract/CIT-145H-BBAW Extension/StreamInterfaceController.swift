//
//  StreamInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

class StreamInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBOutlet var StreamDue: WKInterfaceTable!
    @IBOutlet var StreamAnn: WKInterfaceTable!
    
    var courses = Course.allCourses()//get all the courses per user
    var selectedIndex = 0
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // Configure interface objects here.
        var qty :Int = 0
        var qty2 :Int = 0
        var dueCount: Int = 0
        var annCount: Int = 0
        //Check if assignments or announcements exist per course
        for index in 0..<courses.count {
            if((courses[index].dueJSON.count) != nil) {
                qty += courses[index].dueJSON.count
            }
            if((courses[index].announcementsJSON.count) != nil) {
                qty2 += courses[index].announcementsJSON.count
            }
        }
        //Set the number of row per table into the interface based on qty and qty2
        StreamDue.setNumberOfRows(qty, withRowType: "streamDueRow")
        StreamAnn.setNumberOfRows(qty2, withRowType: "streamAnnRow")
        //Loop through the courses array to obtain the assignment and announcement data
        for index in 0..<courses.count {
            if var due = courses[index].dueJSON as? [[String:AnyObject]], !due.isEmpty {
                
                for index2 in 0..<courses[index].dueJSON.count {
                    
                    guard let dueController = StreamDue.rowController(at: dueCount) as? StreamDueRowController else { continue }
                    //Added coursename to the course dictionary into each array position
                    due[index2]["name"]=courses[index].name as AnyObject
                    dueController.due=due[index2] as AnyObject //pass the data to the RowController
                    dueCount+=1
                }
            }
            if let ann = courses[index].announcementsJSON as? [[String:AnyObject]], !ann.isEmpty {
                
                for index3 in 0..<courses[index].announcementsJSON.count {
                    
                    guard let annController = StreamAnn.rowController(at: annCount) as? StreamAnnRowController else { continue }
                    annController.ann=ann[index3] as AnyObject //pass the data to the RowController
                    annCount+=1
                }
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        // This function can be use to show a modal page with the assignments and announcements information or link to the blackboard website in the iPhone
    }
    
}

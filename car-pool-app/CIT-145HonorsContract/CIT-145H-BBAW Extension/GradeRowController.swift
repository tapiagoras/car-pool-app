//
//  GradeRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/9/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class GradeRowController: NSObject {
    
    //Outlet Definition
    @IBOutlet var finalGrade: WKInterfaceLabel!
    @IBOutlet var totalGrade: WKInterfaceLabel!
    
    // Assign to each row the data passed from GradesDueInterfaceController to each element in the interface
    var grade: AnyObject? {
        didSet {
            guard let grade = grade else { return }
            finalGrade.setText((grade.object(forKey: "finalGrade") as? String))
            totalGrade.setText(grade.object(forKey: "total") as? String)
            
        }
    }
}


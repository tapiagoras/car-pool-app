//
//  InnerMenuInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

// This interface consist in 3 buttons that invoke grades, announcements and Assignments respectively
class InnerMenuInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBAction func gradeButtonPressed() {
        //prepare the context
         let controllers = ["Grades"] // This will invoke the interface Grades
        //present the controller
        presentController(withNames: controllers, contexts: [course as Any, course as Any])
    }
    
    @IBAction func announcementButtonPressed() {
         //prepare the context
        let controllers = ["Announcements"] // This will invoke the interface Announcements
        //present the controller
        presentController(withNames: controllers, contexts: [course as Any, course as Any])
    }
    
    @IBAction func dueButtonPressed() {
         //prepare the context
        let controllers = ["Due"]// This will invoke the interface Due
        //present the controller
        presentController(withNames: controllers, contexts: [course as Any, course as Any])
    }
    
    //This variable hold the information passed from ListCoursesInterfaceController
    var course:Course? {
        didSet {
            guard let course = course else { return }
            setTitle(course.name)
        }
    }
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let course = context as? Course {
            self.course = course
        }
    }
    
    
}

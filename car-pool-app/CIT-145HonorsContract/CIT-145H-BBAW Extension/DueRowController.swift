//
//  DueRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/9/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class DueRowController: NSObject {
    //Outlet Definition
    @IBOutlet var DueTitle: WKInterfaceLabel!
    @IBOutlet var DueDesc: WKInterfaceLabel!
    @IBOutlet var DueDate: WKInterfaceLabel!
    
    // Assign to each row the data passed from ListDueInterfaceController to each element in the interface
    var due: AnyObject? {
        didSet {
            guard let due = due else { return }
            DueTitle.setText((due.object(forKey: "title") as? String))
            DueDesc.setText(due.object(forKey: "description") as? String)
            DueDate.setText(due.object(forKey: "due_date") as? String)
        }
    }
}

//
//  ListDueInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/9/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

//This class control the Assignments Interface

class ListDueInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBOutlet var DueList: WKInterfaceTable!
    
    var course:Course? {
        didSet {
            guard let course = course else { return }
            //Assign the array extracted from the courses.json file to the local due variable
            if let due = course.dueJSON as? [[String:AnyObject]], !due.isEmpty {
                DueList.setNumberOfRows(due.count, withRowType: "DueRow")
                  // this will populate the table AnnouncementsRow with each assignment per selected course
                for index in 0..<DueList.numberOfRows {
                    guard let controller = DueList.rowController(at: index) as? DueRowController else { continue }
                    
                    controller.due = due[index] as AnyObject
                    
                }
                
            }
            
        }
    }
     //extract the context and assign to local variable.
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let course = context as? Course {
            self.course = course
        }
    }
    
}


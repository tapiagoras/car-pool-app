//
//  GradeRowControllerMain.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class GradeRowControllerMain: NSObject {
    //Outlet Definition
    @IBOutlet var courseName: WKInterfaceLabel!
    @IBOutlet var gradeValue: WKInterfaceLabel!
    
     // Assign to each row the data passed from ListGradesInterfaceController to each element in the interface
    var grade: Grade? {
        didSet {
            guard let grade = grade else { return }
            courseName.setText(grade.name)
            gradeValue.setText(grade.grade)
            
        }
    }
}

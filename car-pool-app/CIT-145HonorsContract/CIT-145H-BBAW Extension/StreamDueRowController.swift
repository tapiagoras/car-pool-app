//
//  StreamDueRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class StreamDueRowController: NSObject {
    //Outlets to connect the interface labels in the table DueRow with the data passed from StreamInterfaceController
    @IBOutlet var dueTitle: WKInterfaceLabel!
    @IBOutlet var dueCourse: WKInterfaceLabel!
    @IBOutlet var dueDate: WKInterfaceLabel!
    
    // Assign to each row the data passed from StreamInterfaceController to each element in the interface
    var due: AnyObject? {
        didSet {
            guard let due = due  else { return }
            dueTitle.setText((due.object(forKey: "title") as? String))
            dueCourse.setText(due.object(forKey: "name") as? String)
            dueDate.setText(due.object(forKey: "due_date") as? String)
            
        }
    }
    
}

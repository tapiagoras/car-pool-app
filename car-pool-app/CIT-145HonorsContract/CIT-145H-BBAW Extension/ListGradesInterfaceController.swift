//
//  ListGradesInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

//This class control the ListGradesInterfaceController

class ListGradesInterfaceController: WKInterfaceController {
    
    //Outlet Definition
    @IBOutlet var GradeList: WKInterfaceTable!
    
    var grades = Grade.allGrades()//get all the grades per user
    var selectedIndex = 0
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //Assign the array extracted from the grades.json file to the local grade variable
        // and passed to the table GradesRow
        GradeList.setNumberOfRows(grades.count, withRowType: "GradesRow")
        for index in 0..<GradeList.numberOfRows {
            guard let controller = GradeList.rowController(at: index) as? GradeRowControllerMain else { continue }
            
            controller.grade = grades[index]
            
        }
       
    }
    
   
    
}

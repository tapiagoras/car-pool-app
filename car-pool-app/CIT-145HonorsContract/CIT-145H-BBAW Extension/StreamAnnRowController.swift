//
//  StreamAnnRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class StreamAnnRowController: NSObject {
    
    //Outlets to connect the interface labels in the table AnnRow with the data passed from StreamInterfaceController
    @IBOutlet var AnnTitle: WKInterfaceLabel!
    @IBOutlet var AnnDesc: WKInterfaceLabel!
    
      // Assign to each row the data passed from StreamInterfaceController to each element in the interface
    var ann: AnyObject? {
        didSet {
            guard let ann = ann  else { return }
            AnnTitle.setText((ann.object(forKey: "title") as? String))
            AnnDesc.setText(ann.object(forKey: "description") as? String)
            
        }
    }
    
}

//
//  GradesInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/9/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

//This class control the Inner Grade Interface

class GradesInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBOutlet var GradeList: WKInterfaceTable!
    var course:Course? {
        didSet {
            guard let course = course else { return }
            //Assign the array extracted from the courses.json file to the local grade variable
            if let grade = course.gradeJSON as? [[String:AnyObject]], !grade.isEmpty {
                GradeList.setNumberOfRows(grade.count, withRowType: "GradesRow")
                  // this will populate the table AnnouncementsRow with each grade per selected course
                for index in 0..<GradeList.numberOfRows {
                    guard let controller = GradeList.rowController(at: index) as? GradeRowController else { continue }
                    
                    controller.grade = grade[index] as AnyObject
                    
                }
                
            }
            
        }
    }
     //extract the context and assign to local variable.
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let course = context as? Course {
            self.course = course
        }
    }
    
}



//
//  ListAnnouncementsInterfaceController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit
import Foundation

//This class control the Announcements Interface
//Gets the information value of the course stored in context from InnerMenuInterface
class ListAnnouncementsInterfaceController: WKInterfaceController {
    //Outlet Definition
    @IBOutlet var AnnouncementsList: WKInterfaceTable!
    
    var course:Course? {
        didSet {
            guard let course = course else { return }
            //Assign the array extracted from the courses.json file to the local announcement variable
            if let announcement = course.announcementsJSON as? [[String:AnyObject]], !announcement.isEmpty {
                AnnouncementsList.setNumberOfRows(announcement.count, withRowType: "AnnouncementsRow")
               // this will populate the table AnnouncementsRow with each announcement per selected course
                for index in 0..<AnnouncementsList.numberOfRows {
                    guard let controller = AnnouncementsList.rowController(at: index) as? AnnouncementsRowController else { continue }
                    
                    controller.announcement = announcement[index] as AnyObject
                    
                }
                
            }
            
        }
    }
    
    //extract the context and assign to local variable.
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let course = context as? Course {
            self.course = course
        }
    }
    
}

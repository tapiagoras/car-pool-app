//
//  CourseRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class CourseRowController: NSObject {
     //Outlets to connect the interface labels in the table CourseRow with the data passed from ListCourseInterfaceController
    @IBOutlet var courseName: WKInterfaceLabel!
    @IBOutlet var instructorName: WKInterfaceLabel!
    
    // Assign to each row the data passed from ListCourseInterfaceController to each element in the interface
    var course: Course? {
        didSet {
            guard let course = course else { return }
            courseName.setText(course.name)
            instructorName.setText(course.instructor)
            
        }
    }
}

//
//  AnnouncementsRowController.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

import WatchKit

class AnnouncementsRowController: NSObject {
    //Outlet Definition
    @IBOutlet var AnnouncementTitle: WKInterfaceLabel!
    @IBOutlet var AnnouncementDescription: WKInterfaceLabel!
    // Assign to each row the data passed from ListAnnouncementsInterfaceController to each element in the interface
    var announcement: AnyObject? {
        didSet {
            guard let announcement = announcement else { return }
            AnnouncementTitle.setText((announcement.object(forKey: "title") as? String))
            AnnouncementDescription.setText(announcement.object(forKey: "description") as? String)
            
            
        }
    }
    
}

//
//  Courses.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//
//
// This class define the Grade structure and grab all the data from grades.json file
//
//

import WatchKit
import Foundation

class Grade {
    
    let name: String
    let grade: String
    let gradesJSON: AnyObject
    
    // The allGrades function create an array of dictionary to whole all grades 
    class func allGrades() -> [Grade] {
        var Grades: [Grade] = []
        // this instruction prevent to continue if the json file doesn't exist
        guard let path = Bundle.main.path(forResource: "grades", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
                return Grades
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[AnyHashable: AnyObject]]
            json?.forEach({
                (dict: [AnyHashable: AnyObject]) in Grades.append(Grade(dictionary: dict))
            })
        } catch {
            print(error)
        }
        
        return Grades
    }
    
    //The init method create a new instance of every object is jenerated from the json file
    init(name:String,grade:String, gradesJSON: AnyObject ) {
        self.name = name
        self.grade = grade
        self.gradesJSON = gradesJSON
    }
    
    //Define a convenience initializer to create an instance of the class for a specific use case or input value type
    convenience init(dictionary: [AnyHashable: AnyObject]) {
        let name = dictionary["name"]
        let grade = dictionary["grade"]
        let gradesJSON=dictionary["grades"]
        self.init(name:name as! String,grade:grade as! String,gradesJSON: gradesJSON as AnyObject )
    }
}

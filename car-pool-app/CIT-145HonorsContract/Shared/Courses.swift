//
//  Courses.swift
//  CIT-145HonorsContract
//
//  Created by Fernando Tapia T on 12/8/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//
// This class define the course structure and grab all the data from courses.json file
//
//

import WatchKit
import Foundation

class Course {
    
    let name: String
    let instructor: String
    let announcementsJSON : AnyObject
    let gradeJSON: AnyObject
    let dueJSON: AnyObject
    
    // The allCourses function create an array of dictionary (A collection whose elements are key-value pairs.)
    class func allCourses() -> [Course] {
        var Courses: [Course] = []
        
        // A guard statement is used to transfer program control out of a scope if one or more conditions aren’t met
        // this instruction prevent to continue if the json file doesn't exist
        guard let path = Bundle.main.path(forResource: "courses", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
                return Courses
        }
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[AnyHashable: AnyObject]]
            json?.forEach({
                (dict: [AnyHashable: AnyObject]) in Courses.append(Course(dictionary: dict))
                
            })
        } catch {
            print(error)
        }
        
        return Courses
    }
    
    //The init method create a new instance of every object is jenerated from the json file
    init(name:String,instructor:String, announcementsJSON: AnyObject,gradeJSON:AnyObject,dueJSON:AnyObject ) {
        self.name = name
        self.instructor = instructor
        self.announcementsJSON = announcementsJSON
        self.gradeJSON = gradeJSON
        self.dueJSON = dueJSON
        
    }
    
    //Define a convenience initializer to create an instance of the class for a specific use case or input value type
    convenience init(dictionary: [AnyHashable: AnyObject]) {
        
        let name = dictionary["name"]
        let instructor = dictionary["faculty"]
        let announcementsJSON=dictionary["announcements"]
        let gradeJSON=dictionary["grade"]
        let dueJSON=dictionary["due"]
        self.init(name:name as! String,instructor:instructor as! String, announcementsJSON: announcementsJSON as AnyObject, gradeJSON: gradeJSON as AnyObject , dueJSON: dueJSON as AnyObject )
    }
}

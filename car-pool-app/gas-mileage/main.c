//
//  main.c
//  gas-mileage
//
//  Created by Fernando Tapia T on 10/4/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    //Define variables
    int count=0;
    double galUsed=0,milesDriven,galMileAve=0;
    //GetUserInput
    while(galUsed!= -1){
        printf("%s","Enter the gallons used (-1 to end): ");
        scanf("%lf", &galUsed);
        if (galUsed == -1) break;
        printf("%s","Enter the miles driven: ");
        scanf("%lf", &milesDriven);
        galMileAve+=(milesDriven/galUsed);
        printf("The miles/gallon for this tank was %lf \n\n",milesDriven/galUsed);
        count++;
    }
    
    printf("\nThe overall average miles/gallon was %lf \n",galMileAve/count);
    
    return 0;
}

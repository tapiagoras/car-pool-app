//
//  main.c
//  week5InClass3
//
//  Created by Fernando Tapia T on 10/5/17.
//  Copyright � 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(void){
    //declare variable
    int score;
    
    //for loop
    for (score =0; score<=18; score+=6)
    {
        printf("The score of the game is %d \n", score);
    }
    
    return 0;

}

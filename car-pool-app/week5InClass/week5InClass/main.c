//
//  main.c
//  week5InClass
//
//  Created by Fernando Tapia T on 10/5/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(void) {
    int number;
    printf("\nEnter a number between 2 and 20: ");
    scanf ("%d",&number);
    
    switch(number){
        case 2:
            printf("The Baltimore Ravens  have only been in 2 Super Bowls\n");
            break;
        
        case 8:
            printf("The Steelers have been in 8 Super Bowls\n");
            break;
        
        default:
            printf("That selection is not available \n");
            break;
        
            
    }
    
}

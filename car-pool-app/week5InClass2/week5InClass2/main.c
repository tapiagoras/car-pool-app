//
//  main.c
//  week5InClass2
//
//  Created by Fernando Tapia T on 10/5/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(void) {
    int day=1;
    float amount=.02;
    while (day<=31){
        printf("Day:%d \t Amount:%.2f \n",day,amount);
        amount*=2;
        day++;
        
    }
    return 0;
}

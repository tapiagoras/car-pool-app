//
//  main.c
//  car-pool-app
//
//  Created by Fernando Tapia T on 9/25/17.
//  Copyright © 2017 Fernando Tapia T. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    //Define variables
    int milesPerDay, milesPerGal;
    double costPerGal,costPerDay,parkFees,tollPerDay;
    //GetUserInput
    printf("%s","Enter Total miles driven per day --> ");
    scanf("%d", &milesPerDay);
    printf("%s","Enter Cost per gallon of gasoline--> ");
    scanf("%lf", &costPerGal);
    printf("%s","Enter Averages miles per gallon --> ");
    scanf("%d", &milesPerGal);
    printf("%s","Enter Parking fees per day --> ");
    scanf("%lf", &parkFees);
    printf("%s","Enter Tolls per day --> ");
    scanf("%lf", &tollPerDay);
    costPerDay=(costPerGal/milesPerGal)*milesPerDay+parkFees+tollPerDay;
    printf("the user’s cost per day of driving to work is %f ",costPerDay);
    
    return 0;
}
